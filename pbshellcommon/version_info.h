#ifndef __VERSION_INFO_H__
#define __VERSION_INFO_H__

#define COMPANYNAME          "Pitney Bowes"
#define SUBSYSTEMDESCRIPTION "MS1 pbshell"
#define SUBSYSTEMVERSION	"00.02.0006.0000"
#define INTERNALNAME         "PBShell"
#define LEGALCOPYRIGHT       "(c) Pitney Bowes Inc.  All rights reserved."
#define ORIGINALFILENAME     "pbshell"
#define PRODUCTNAME          "Digital Software and Data Distribution"
#define PRODUCTVERSION	      "00.02.*.*"
#define PRODBUILDDATE	"2019-02-01 19:00:57"

#endif // VERSION_INFO_H__
